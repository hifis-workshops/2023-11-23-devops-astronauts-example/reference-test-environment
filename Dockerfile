# SPDX-FileCopyrightText: 2023 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

# Use the official Python 3.10 image as the base image
FROM python:3.10

# Add the 'requirements.txt' file from the repository
ADD requirements.txt .

# Install the Python packages specified in 'requirements.txt' using pip
RUN pip install -r requirements.txt

# Set the working directory inside the container to '/app'
WORKDIR /app
