<!--
SPDX-FileCopyrightText: 2023 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

# Astronauts Analysis Reference Test Environment

## Overview

Welcome to the Astronauts Analysis Reference Test Environment repository. This project is dedicated to maintaining a consistent and reliable test environment for analyzing astronaut data, ensuring a standardized framework for development and testing.

## Repository Structure

```
reference-test-environment
│   .gitlab-ci.yml         # CI/CD pipeline configuration file for GitLab.
│   Dockerfile             # Dockerfile to build the Python environment.
│   README.md              # The file you are currently reading.
└── requirements.txt       # Required Python packages and tools for the environment.
```

## Environment Details

- **Python 3.10**: We use Python version 3.10 running on Linux to guarantee the application's performance and backward compatibility.
- **Linter and Check Tools**: Our `requirements.txt` includes all the necessary linting and checking tools to ensure code quality and standards are met.

## CI/CD Pipeline

The GitLab CI/CD pipeline is configured to build a Docker container image upon every commit, automating the creation and deployment of the testing environment. This image is then stored and made available through the built-in Docker registry of GitLab, ready to be pulled and used for testing purposes.

## Quickstart

1. **Clone the repository**:
   ```bash
   git clone https://codebase.helmholtz.cloud/hifis-workshops/2023-11-23-devops-astronauts-example/reference-test-environment
   ```

2. **Build the Docker image**:
   ```bash
   docker build -t astronauts-analysis-env .
   ```

3. **Run the container**:
   ```bash
   docker run -it astronauts-analysis-env
   ```

## Contributions

Contributions to this project are more than welcome. Please ensure you adhere to our coding standards and commit guidelines.

## License

Please see the file [LICENSE.md](LICENSE.md) for further information about how the content is licensed.
